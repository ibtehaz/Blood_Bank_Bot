# Generated by Django 2.0.1 on 2018-03-10 21:59

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blood_bank', '0007_auto_20180310_2137'),
    ]

    operations = [
        migrations.RenameField(
            model_name='datadump',
            old_name='recordedDate',
            new_name='recordedTime',
        ),
        migrations.RenameField(
            model_name='datadump',
            old_name='senderIdentity',
            new_name='userID',
        ),
    ]
