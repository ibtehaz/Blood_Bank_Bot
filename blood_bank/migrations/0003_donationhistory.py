# Generated by Django 2.0.1 on 2018-01-29 06:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('blood_bank', '0002_usertable'),
    ]

    operations = [
        migrations.CreateModel(
            name='DonationHistory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('donatedOn', models.CharField(default=-1, max_length=30, null=True)),
                ('userID', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='blood_bank.UserTable')),
            ],
        ),
    ]
