import binascii
import os
from time import gmtime, strftime

from django.core.exceptions import ObjectDoesNotExist

from blood_bank.models import UserTable
from blood_bank.serializer import DumpMessageSerializer, LoggerSerializer, UserSerializer
from blood_bank.utility import nlp_parser

TAG_TEXT = 'incomingText'
TAG_USER_ID = 'userID'
TAG_TIME = 'recordedTime'
TAG_CONFIDENCE_BYE = 'confidenceBye'
TAG_CONFIDENCE_GREET = 'confidenceGreet'
TAG_CONFIDENCE_THANK = 'confidenceThank'
TAG_CONFIDENCE_LOC = 'confidenceLoc'
TAG_ERROR_MESSAGE = "errorMessage"
TAG_ERROR_CODE = 'errorCode'
TAG_ERROR_SUBCODE = 'errorSubCode'
TAG_ERROR_TYPE = 'errorType'
TAG_ERROR_PLACE = 'errorPlace'
TAG_FB_USERID = 'facebookUserID'
TAG_USER_TABLE_ID = 'userID'
TAG_USER_BLOOD_GRP = 'bloodGroup'
TAG_USER_MOBILE_NUMBER = 'mobileNumber'
TAG_MOBILE_VERIFIED = 'mobileVerified'
TAG_USER_HOME = 'homeCity'
TAG_USER_CURRENT = 'currentCity'

TAG_MESSAGE_TYPE = 'messageType'
TAG_BASIC_TYPE = 'basic_reply'
TAG_QUICK_TYPE = 'quick_reply'
TAG_NLP_TYPE = 'nlp_reply'

"""
insert_queue
message insertion queue in the database
"""


def insert_queue(message_data):
    message_text = message_data['message']['text']
    user_id = message_data['sender']['id']
    bye_val = 0.00
    greet_val = 0.00
    thank_val = 0.00
    loc_val = 0.00

    if 'nlp' in message_data['message']:
        message_type = TAG_NLP_TYPE
    else:
        message_type = TAG_BASIC_TYPE

    if 'bye' in message_data['message']['nlp']['entities']:
        bye_val = nlp_parser(message_data, 'bye')
    if 'thanks' in message_data['message']['nlp']['entities']:
        thank_val = nlp_parser(message_data, 'thanks')
    if 'greetings' in message_data['message']['nlp']['entities']:
        greet_val = nlp_parser(message_data, 'greetings')
    if 'location' in message_data['message']['nlp']['entities']:
        loc_val = nlp_parser(message_data, 'location')

    if len(message_text) > 10000:
        message_text = message_text[0:10000]

    payload = {
        TAG_USER_ID: user_id,
        TAG_TEXT: message_text,
        TAG_TIME: strftime("%Y-%m-%d %H:%M:%S", gmtime()),
        TAG_CONFIDENCE_BYE: bye_val,
        TAG_CONFIDENCE_GREET: greet_val,
        TAG_CONFIDENCE_LOC: loc_val,
        TAG_CONFIDENCE_THANK: thank_val,
        TAG_MESSAGE_TYPE: message_type
    }
    serialized_data = DumpMessageSerializer(data=payload)
    if serialized_data.is_valid():
        serialized_data.save()
        return 1
    else:
        error_message = serialized_data.error_messages()
        error_logger(error_message, None, user_id, None, None, 'insert_queue')
        return -1


"""
error_logger
logs every error in the database
"""


# noinspection SpellCheckingInspection
def error_logger(error_message, error_code, facebook_id, error_subcode, error_type, error_position):
    if facebook_id is not None:
        db_user_id = find_actual_user_id(facebook_id)
    if error_code is None:
        error_code = -1
    if error_subcode is None:
        error_subcode = -1
    if error_type is None:
        error_type = -1
    payload = {
        TAG_USER_ID: db_user_id,
        TAG_ERROR_MESSAGE: error_message,
        TAG_ERROR_CODE: error_code,
        TAG_ERROR_SUBCODE: error_subcode,
        TAG_ERROR_TYPE: error_type,
        TAG_ERROR_PLACE: error_position
    }
    serialized_data = LoggerSerializer(data=payload)
    if serialized_data.is_valid():
        serialized_data.save()
        return 1
    else:
        print("error logging error! oh crap! " + str(serialized_data.error_messages))
        return -1


"""
unique_user_check
This function will check and return boolean whether user_id is unique in the table or not.
user_id is facebook user id on the table -> UserTable
"""


def unique_user_check(user_id):
    try:
        request_query = UserTable.objects.filter(facebookUserID=user_id)
        if request_query.count() == 0:
            return True
        else:
            return False
    except ObjectDoesNotExist as obj:
        print("error occurred in unique user check")
        error_logger(str(obj), None, user_id, None, None, 'unique_user_check')
        return True


"""
user_table_insertion
This function will create a new user on the user table, based on userID. if the user is new.
"""


def user_table_insertion(user_id):
    if not unique_user_check(user_id):
        return -1
    database_user_id = str(binascii.hexlify(os.urandom(4)))
    payload = {
        TAG_USER_TABLE_ID: database_user_id,
        TAG_FB_USERID: user_id,
    }
    serialized_data = UserSerializer(data=payload)
    if serialized_data.is_valid():
        serialized_data.save()
        return 1
    else:
        error_message = serialized_data.error_messages()
        error_logger(error_message, None, user_id, None, None, 'user_table_insertion')
        return -1


"""
update_user_table
this function will update the existing users data
"""


def update_user_table(user_id, blood_group, mobile_number, mobile_verify, home, current_loc):
    if unique_user_check(user_id):
        request_query = UserTable.objects.get(facebookUserID=user_id)
        if blood_group is not None:
            request_query.bloodGroup = blood_group
        if mobile_number is not None:
            request_query.mobileNumber = mobile_number
        if mobile_verify is not None:
            request_query.mobileVerified = mobile_verify
        if home is not None:
            request_query.homeCity = home
        if current_loc is not None:
            request_query.currentCity = current_loc
        request_query.save()
        return 1
    else:
        return -1


"""
find_actual_user_id
this function will return the actual user id of a user in the database from the facebook id.
"""


def find_actual_user_id(user_id):
    try:
        request_query = UserTable.objects.get(facebookUserID=user_id)
        return request_query.userID
    except ObjectDoesNotExist as obj:
        print("ObjectDoesNotExist occurred in find_actual_user_id "+str(obj))
        error_logger(str(obj), None, user_id, None, None, 'find_actual_user_id')
        return None
    except AttributeError as attr:
        print("AttributeError occurred in find_actual_user_id " + str(attr))
        error_logger(str(attr), None, user_id, None, None, 'find_actual_user_id')
        return None
    except TypeError as terr:
        print("TypeError occurred in find_actual_user_id " + str(terr))
        error_logger(str(terr), None, user_id, None, None, 'find_actual_user_id')
        return None
